import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const notGoodSchema = new Schema ({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  anime: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Anime'
  },
  rating: {
    type: Number,
    default: 5
  },
}, {timestamps: true})

export default mongoose.model('Notgood', notGoodSchema)
