import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const Schema = mongoose.Schema;

const userSchema = new Schema ({
  username: {
    type: String,
    default: 'Anime Fan'
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  admin: {
    type: Boolean,
    default: false
  },
  tokens: {
    type: Array,
    of: String
  }
})

userSchema.pre('save', async function(next) {
  const user = this;

  const hashPassword = await bcrypt.hash(user.password, 10);
  
  this.password = hashPassword;
  next();
})

userSchema.methods.isValidPassword = async function(password) {
  const user = this;

  const comp = bcrypt.compare(password, user.password);

  return comp
}

export default mongoose.model('User', userSchema);
