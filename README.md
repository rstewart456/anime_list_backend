# anime_list_backend

## About
This is the backend of the Favorite Anime List app. I wanted a web app to search for anime and to have three main lists. The list are My Favorites, Not Seen yet and Not My Favorite. There is a rating for each anime in the lists. It will help with the order in each list. I decided to go with Graphql because anilist uses it and I wanted to try somthing new.

## Some of the Technologies are in the app.
- Apollo Server Express
- JSON web token
- Mongoose

## Author
Ronald Stewart
I am Anime Addict. I have watch anime for over 20 years. Furthermore, I wanted list that I can keep track of my favorites, shows I don't like, and shows I have seen yet. I don't want to forget the name of some of my favorite anime.

## To Do
- [X] Create Favorite List
- [X] Create Not Seen List
- [X] Create Not Good List
- [X] Search anilist for anime
- [ ] Search throught the Pages on anilist Anime
- [X] Log In
- [X] Sign UP
- [X] Assign Token
- [ ] verify token is in the user tokens list
- [ ] Clean up files
- [ ] Document earch function