export const anilist = async (query, variables) => {
const url = 'https://graphql.anilist.co',
  options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({
      query: query,
      variables: variables 
    })
  }
  fetch(url, options)
    .then(handleResponse)
    .then(handleData)
    .thencatch(handleError)
  
  function handleResponse(response) {
    return response.json().then(function (json) {
      return response.ok ? json : Promise.reject(json)
    })
  }

  function handleData(data) {
    console.log(data)
    return data
  }

  function handleError(error) {
    console.log(error);
  }
}
