import cors from 'cors';

//const whitelist = ['https://rstewart456react.duckdns.org', 'https://rstewart456api.duckdns.org'];
//const whitelist = ['http://192.168.1.20:3000','http://localhost:3000', 'http://localhost:3443'];
const whitelist = ['http://localhost:3000', 'http://localhost:3443'];
export const corsOptionsDelegate = (req, callback) => {
  let corsOptions;
  if (whitelist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { origin: true, credentials: true, exposedHeaders: ['Authorization'], };
  } else {
    corsOptions = { origin: false }
  }
  callback(null, corsOptions);
}

export const cor = cors(corsOptionsDelegate);

