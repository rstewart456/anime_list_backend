import dotenv from 'dotenv';
import express from 'express';
import { verifyAccess, bearerExtractor } from './config/authenicate.js';
import { connectDB } from './config/db.js';
import cookieParser from 'cookie-parser';
import { cor } from './config/cors.js';
import { ApolloServer } from 'apollo-server-express';
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core';
import http from 'http';
import { typeDefs } from './apollo/typedefs.js';
import { resolvers } from './apollo/resolvers.js';

dotenv.config();

const app = express();

const httpServer = http.createServer(app);

app.use(cookieParser());

const server = new ApolloServer({
  typeDefs,
  resolvers,
  cor,
  csrfPrevention: true,
  context: async ({ req, res }) => {

    let ctx = {
      req,
      res,
      id: null,
      token: null,
    }

    const verify = await verifyAccess(req)
    const bearer = await bearerExtractor(req)
    if (verify.id) {
      ctx.id = verify.id
      ctx.token = bearer
    }
    return ctx
  },
  plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
});

const start = async () => {
  await connectDB(process.env.MONGO_URL);
  await server.start();
  server.applyMiddleware({ app, path: '/' })
  await new Promise(resolve => httpServer.listen({ port: 3443 }, resolve));
  console.log(`Server Ready at port 3443...${server.graphqlPath}`);
}

start();
