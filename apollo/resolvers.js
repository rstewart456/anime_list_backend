import { AuthenticationError } from "apollo-server-express";
import { accessToken, bearerExtractor } from "../config/authenicate.js";
import User from "../models/user.js";
import Anime from "../models/anime.js";
import Favorite from "../models/favorite.js";
import Notseen from "../models/notseen.js";
import Notgood from "../models/notgood.js";
import axios from "axios";

export const resolvers = {
  Query: {
    users: async (_, __, context) => {
      const user = await User.findById(context.id);
      if (user.admin) {
        const user = await User.find();
        return user;
      } else {
        throw new AuthenticationError(
          "You are not Authorize for this operations"
        );
      }
    },
    me: async (_, __, context) => {
      const user = await User.findById(context.id);
      if (!user) {
        throw new AuthenticationError('No user is Found');
      } else {
        return {
          id: user._id,
          username: user.username,
          email: user.email,
        };
      }
    },
    animes: async () => {
      const animes = await Anime.find();
      return animes;
    },
    anime: async (_, args) => {
      const anime = await Anime.findOne({ _id: args.id });
      return anime;
    },
    searches: async (_, args) => {
      const endpoint = "https://graphql.anilist.co";
      const headers = {
        "content-type": "application/json",
      };
      const graphqlQuery = {
        query: `
        query ($id: Int, $page: Int, $perPage: Int, $search: String!) {
          Page (page: $page, perPage: $perPage) {
            pageInfo {
            total
            currentPage
            lastPage
            hasNextPage
            perPage
            }
          media(id: $id, search: $search, type: ANIME) {
            id
            title {
              romaji
              english
            }
            coverImage {
            extraLarge
            }
            description
            seasonYear
            episodes
            genres
          }
        }
        }
      `,
        variables: {
          search: args.search,
          page: args.page,
          perPage: args.perPage,
        },
      };
      const response = await axios({
        url: endpoint,
        method: "POST",
        headers: headers,
        data: graphqlQuery,
      });
      let animeArray = []

      const anime = response.data.data.Page.media
      // For Loop to convert Anilist Naming to My Naming System.
      for(let i = 0; i < anime.length; ++i) {
        
        let titleName = ''

        if(anime[i].title.english == undefined) {
          titleName = anime[i].title.romaji
        } else {
          titleName = anime[i].title.english
        }

        const ani = {
          id: anime[i].id,
          title: titleName,
          anilistId: anime[i].id,
          image: anime[i].coverImage.extraLarge,
          description: anime[i].description,
          release: anime[i].seasonYear,
          episodes: anime[i].episodes,
          genres: anime[i].genres
        }
        animeArray.push(ani)
      }

      const fetchAnime = {
        total: response.data.data.Page.pageInfo.total,
        currentPage: response.data.data.Page.pageInfo.currentPage,
        lastPage: response.data.data.Page.pageInfo.lastPage,
        hasNextPage: response.data.data.Page.pageInfo.hasNextPage,
        perPage: response.data.data.Page.pageInfo.perPage,
        animes: animeArray
      };

      console.log("Hello World")
      return fetchAnime;
    },
    anilist: async (_, args) => {
      const endpoint = "https://graphql.anilist.co";
      const headers = {
        "content-type": "application/json",
      };
      const graphqlQuery = {
        query: `
        query ($id: Int) {
          Media(id: $id type: ANIME) {
            id
            title {
              english
            }
            coverImage {
            extraLarge
            }
            description
            seasonYear
            episodes
            genres
          }
        }
      `,
        variables: {
          id: args.id,
        },
      };
      const response = await axios({
        url: endpoint,
        method: "POST",
        headers: headers,
        data: graphqlQuery,
      });

      const fetchAnime = {
        id: response.data.data.Media.id,
        title: response.data.data.Media.title.english,
        genre: response.data.data.Media.genres,
        image: response.data.data.Media.coverImage.extraLarge,
        description: response.data.data.Media.description,
        release: response.data.data.Media.seasonYear,
        episodes: response.data.data.Media.episodes,
      };
      return fetchAnime;
    },
    favorite: async (_, __, context) => {
      const favorite = await Favorite.find({ user: context.id }).populate(
        "anime"
      );
      return favorite;
    },
    notseen: async (_, __, context) => {
      const notseen = await Notseen.find({ user: context.id }).populate(
        "anime"
      );
      return notseen;
    },
    notgood: async (_, __, context) => {
      const notgood = await Notgood.find({ user: context.id }).populate(
        "anime"
      );
      return notgood;
    },
  },
  Mutation: {
    addUser: async (_, args) => {
      try {
        // Checks to see if any one has a email setup.
        const check = await User.findOne({ email: args.email });
        if (check) {
          new AuthenticationError("This email is already in the Database");
        } else {
          // Creates a new user in the Database.
          await User.create(args);
          // Find user
          const user = await User.findOne({ email: args.email });
          // Creates a new token
          const token = accessToken(user);
          await User.updateOne(
            { email: args.email },
            { $push: { tokens: token } }
          );
          return {
            id: user._id,
            username: args.username,
            email: args.email,
            token: token,
          };
        }
      } catch (e) {
        return onsole.log(e);
      }
    },
    logIn: async (_, args, context) => {
      try {
        // Finds User in the Databse
        const user = await User.findOne({ email: args.email });
        // Fetches the Refresh Tokens
        const fetchtokens = user.tokens;
        // Have Issue with Changes Mongoose and Mongodb array directly.
        // I just Copy the Array to a new one and then push the new array to the Database.
        // Copies the Array from the Refresh Tokens
        const fetchCopy = [...fetchtokens];
        // Gets the Refresh Tokens From the Cookie
        const getCookie = bearerExtractor(context.req);
        // Get the Index of the Refresh Token from the Copy Array
        const getIndex = fetchCopy.indexOf(getCookie);
        // Creates a new Refresh Tokens
        const token = accessToken(user);
        // If there is not a user in the Database.
        if (!user) {
          new AuthenticationError("No user with this email is found");
          console.log("No user Found");
        }
        // Checks the password against the password in the Database.
        const valid = await user.isValidPassword(args.password);
        // If the password is not valid
        if (!valid) {
          new AuthenticationError("Wrong Password");
          console.log("Password not Valid");
          // Checks to see the Cookie Tokens matches any of the tokens in user Database.
        } else if (getIndex >= 0) {
          // Pushes a new refresh to the copy Array
          fetchCopy.push(token);
          // Slices out the out Token from the copy Array
          fetchCopy.splice(getIndex, 1);
          // It replaces the Old Tokens with the Copy Tokens to the user Database.
          await User.updateOne({ email: args.email }, { tokens: fetchCopy });
          // Returns the user Information
          return {
            id: user._id,
            email: user.email,
            username: user.username,
            token: token,
          };
          // Checks to see if only five Tokens is in the user Database.
        } else if (fetchCopy > 4) {
          throw new AuthenticationError(
            "Reset you devices to use this new device"
          );
        } else {
          console.log(token);
          await User.updateOne(
            { email: args.email },
            { $push: { tokens: token } }
          );
          return {
            id: user._id,
            username: user.username,
            email: user.email,
            token: token,
          };
        }
      } catch (e) {
        console.log(e);
      }
    },
    // Adds a new Anime to the Database
    addAnime: async (_, args) => {
      try {
        const anime = await Anime.findOne({ title: args.title });
        if (anime) {
          return anime;
        } else {
          let response = await Anime.create(args);
          return response;
        }
      } catch (e) {
        console.log(e);
      }
    },
    // Adds the Anime to the User Favorite List
    addFavorite: async (_, args, context) => {
      try {
        await Favorite.deleteMany({ user: context.id, anime: args.anime });
        await Notseen.deleteMany({ user: context.id, anime: args.anime });
        await Notgood.deleteMany({ user: context.id, anime: args.anime });
        let response = await Favorite.create({
          user: context.id,
          anime: args.anime,
        });
        return response;
      } catch (e) {
        console.log(e);
      }
    },
    // Adds the Anime to the User Not Seen List
    addNotseen: async (_, args, context) => {
      try {
        await Notseen.deleteMany({ user: context.id, anime: args.anime });
        await Favorite.deleteMany({ user: context.id, anime: args.anime });
        await Notgood.deleteMany({ user: context.id, anime: args.anime });
        let response = await Notseen.create({
          user: context.id,
          anime: args.anime,
        });
        return response;
      } catch (e) {
        console.log(e);
      }
    },
    // Adds the Anime to the User Not Good List
    addNotgood: async (_, args, context) => {
      try {
        await Notgood.deleteMany({ user: context.id, anime: args.anime });
        await Favorite.deleteMany({ user: context.id, anime: args.anime });
        await Notseen.deleteMany({ user: context.id, anime: args.anime });
        let response = await Notgood.create({
          user: context.id,
          anime: args.anime,
        });
        return response;
      } catch (e) {
        console.log(e);
      }
    },
    // Removes the Anime from the User Favorite List
    removeFavorite: async (_, args) => {
      try {
        await Favorite.deleteOne({ _id: args.id }).populate("anime");
        return { message: "The Anime was Delete From your Favorite List" };
      } catch (e) {
        console.log(e);
      }
    },
    // Removes the Anime from the User Not Seen List
    removeNotseen: async (_, args) => {
      try {
        await Notseen.deleteOne({ _id: args.id });
        return { message: "You anime was Delete from your Not Seen List" };
      } catch (e) {
        console.log(e);
      }
    },
    // Remoes the Anime from the User Not Good List
    removeNotgood: async (_, args) => {
      try {
        await Notgood.deleteOne({ _id: args.id });
        return { message: "Your Anime was Delete from your Not Good List" };
      } catch (e) {
        console.log(e);
      }
    },
  },
};
