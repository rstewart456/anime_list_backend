import { gql } from 'apollo-server-express';

export const typeDefs = gql`
type User {
  id: ID!
  username: String
  email: String
  admin: Boolean
  token: String
},
type Anime {
  id: ID!
  anilistId: Int
  title: String
  image: String
  description: String
  genres: [String]
  release: Int
  episodes: Int
},
type Favorite {
  id: ID!
  user: String
  anime: Anime
  rating: Int
},
type Notseen {
  id: ID!
  user: String
  anime: Anime
  rating: Int
},
type Notgood {
  id: ID! 
  user: String
  anime: Anime
  rating: Int
},
type Message {
  message: String
}
type Search {
  id: ID!
  total: Int
  currentPage: Int
  lastPage: Int
  hasNextPage: Boolean
  perPage: Int
  search: String!
  animes: [Anime]
}
type Anilist {
  id: ID!
  title: String
  image: String
  description: String
  genres: [String]
  release: Int
  episodes: String
}
type Query {
  me: User
  users: [User]
  anime(id: ID!): Anime
  animes: [Anime]
  searches(search: String!, page: Int, perPage: Int): Search
  anilist(id: Int!): Anilist
  favorite: [Favorite]
  notseen: [Notseen]
  notgood: [Notgood]
  message: Message
},
type Mutation {
  addUser(username: String!, email: String!, password: String! confirmPassword: String!): User,
  addAnime(anilistId: Int!, title: String!, image: String, description: String, genres: [String], release: Int, episodes: Int): Anime,
  searchAnime(search: String!, page: Int, perPage: Int): Search
  logIn(email: String!, password: String!): User,
  addFavorite(anime: String!): Favorite,
  addNotseen(anime: String!): Notseen,
  addNotgood(anime: String!): Notgood,
  removeFavorite(id: String! message: String): Message,
  removeNotseen(id: String! message: String): Message
  removeNotgood(id: String! message: String): Message
}
`
